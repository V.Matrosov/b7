#!/usr/bin/env python3

import sys
import cgi
import cgitb
import codecs
import html

import psycopg2
from psycopg2 import sql

from user import User

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

form = cgi.FieldStorage()
if form.getvalue('field-username'):
    username = html.escape(form.getvalue('field-username'))
else:
    username = ''
if form.getvalue('field-password'):
    password = html.escape(form.getvalue('field-password'))
else:
    password = ''
new_user = None
if password == '' or username == '':
    print('Set-cookie: aaaa=aaaa')
    print("Content-type: text/html")
    print()
    print('''<DOCTYPE html>
    <html lang="ru">
    <head>
    <title>backend-4</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
    <h1>Введите заново логин и пароль</h1>
    <form action="/cgi-bin/login.py" method="post">
        <label>Username:<br><input name="field-username"></label><br>
        <label>Password:<br><input name="field-password" type="password"></label><br>
        <input type="submit" class="btn-primary" value="Отправить">
    </form>
    </body>
    </html>
    ''')
    exit()
else:
    new_user = User(username, password)

e = False
if new_user is not None:
    conn = psycopg2.connect(
        dbname='postgres',
        user='myuser',
        password='UigfjL0f',
        host='localhost'
    )
    with conn.cursor() as cursor:
        conn.autocommit = True
        usnm = new_user.get_username()
        pswd = new_user.get_password()
        values = [
            (0, usnm, pswd)
        ]
        insert = sql.SQL(
            'INSERT INTO users (id_user, username, password) VALUES {}').format(
            sql.SQL(',').join(map(sql.Literal, values))
        )
        cursor.execute(insert)
    conn.close()

print("Content-type: text/html")
print()
print(f'''<DOCTYPE html>
    <html lang="ru">
    <head>
    <title>backend-4</title>
    <meta charset="UTF-8">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
    <h1>Регистрация прошла успешно</h1>
    <a href="/backend-5/login.html" class="btn btn-warning">Авторизация</a>
    </body>
    </html>''')