#!/usr/bin/env python3

import cgi
import cgitb
import codecs
import datetime
import html
import re
import sys
import http.cookies

import psycopg2
from psycopg2 import sql

# Кодировка utf-8

cgitb.enable()

sys.stdout = codecs.getwriter("utf-8")(sys.stdout.detach())

'''Обработка формы'''
f = False
form = cgi.FieldStorage()
text1 = form.getvalue("field-name-1", "")
cookie = {'name': text1}
text1 = html.escape(text1)
text2 = form.getvalue("field-email", "")
cookie['email'] = text2
text2 = html.escape(text2)
date1 = datetime.datetime.strptime(form.getvalue("field-date"), "%Y-%m-%d")
date1 = date1.date()
cookie['date'] = date1.strftime("%Y-%m-%d")
sex = form.getvalue('radio-group-1')
cookie['sex'] = sex
count = int(form.getvalue('radio-group-2'))
cookie['count'] = str(count)
subject = form.getvalue('field-name-4')
cookie['subject'] = subject
bio = form.getvalue('field-name-2')
if not bio is None:
    bio = html.escape(bio)
    cookie['bio'] = bio
else:
    bio = ''
if form.getvalue('check-1'):
    checkbox_flag = True
    cookie['check'] = 'on'
else:
    checkbox_flag = False
    cookie['check'] = 'off'

conn = psycopg2.connect(
    dbname='postgres',
    user='myuser',
    password='UigfjL0f',
    host='localhost'
)
with conn.cursor() as cursor:
    conn.autocommit = True
    insert = sql.SQL(f"DELETE FROM form WHERE first_name = '{text1}'")
    cursor.execute(insert)
    values = [
        (0, text1, text2, date1, sex, count, subject, bio, checkbox_flag),
    ]
    insert = sql.SQL(
        'INSERT INTO form (id_person, first_name, email, nowdate, sex, kolvo, abilities, bio, checkbox) VALUES {}').format(
        sql.SQL(',').join(map(sql.Literal, values))
    )
    cursor.execute(insert)
conn.close()

print("Content-type: text/html")
print()
print('''
    <!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="UTF-8">
        <title>Авторизация</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    </head>
    <body>
        <h1>Изменения успешно внесены</h1>
        <a href="profile.py" class="btn btn-warning">Профиль</a>
    </body>
</html>
''')
